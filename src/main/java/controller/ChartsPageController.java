package controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import model.CurrencyExchangeRecord;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import service.CurrencyExchangeDataProviderService;

import javax.annotation.Resource;

@Controller
@RequestMapping("/chart")
public class ChartsPageController {

    @Resource
    private CurrencyExchangeDataProviderService currencyExchangeDataProviderService;

    private static final String START_DATE_PARAM = "startDate";
    private static final String END_DATE_PARAM = "endDate";
    private static final String FIRST_AVAILABLE_DATA_DATE_PARAM = "firstAvailableDataDate";
    private static final String LAST_AVAILABLE_DATA_DATE_PARAM = "lastAvailableDataDate";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getChartsPage(
            @RequestParam(value = START_DATE_PARAM, required = false)
                @DateTimeFormat(pattern="${date.format.pattern}") Date startDate,
            @RequestParam(value = END_DATE_PARAM, required = false)
                @DateTimeFormat(pattern="${date.format.pattern}") Date endDate,
            ModelMap model)
    {
        // get all available currencyExchangeData for specified date range sorted by date
        List<CurrencyExchangeRecord> data = currencyExchangeDataProviderService.getDataForPeriod(startDate, endDate);
        Collections.sort(data, new Comparator<CurrencyExchangeRecord>() {
            @Override
            public int compare(CurrencyExchangeRecord o1, CurrencyExchangeRecord o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });

        // fill the model
        model.addAttribute(START_DATE_PARAM, startDate);
        model.addAttribute(END_DATE_PARAM, endDate);
        model.addAttribute(FIRST_AVAILABLE_DATA_DATE_PARAM, data.get(0).getDate());
        model.addAttribute(LAST_AVAILABLE_DATA_DATE_PARAM, data.get(data.size() - 1).getDate());

        // render view (chartsPage,jsp)
        return "chartsPage";
    }

    @RequestMapping(value = "/data", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<CurrencyExchangeRecord> getChartData(
            @RequestParam(value = START_DATE_PARAM, required = false)
                @DateTimeFormat(pattern="${date.format.pattern}") Date startDate,
            @RequestParam(value = END_DATE_PARAM, required = false)
                @DateTimeFormat(pattern="${date.format.pattern}") Date endDate)
    {
        return currencyExchangeDataProviderService.getDataForPeriod(startDate, endDate);
    }

}