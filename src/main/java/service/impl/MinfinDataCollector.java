package service.impl;

import model.CurrencyExchangeRecord;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import service.CurrencyExchangeDataProviderService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Collect historical data about the currency exchange rates from the http://minfin.com.ua web site.
 */
@Service
public class MinfinDataCollector implements CurrencyExchangeDataProviderService {

    @Resource
    private RestTemplate restTemplate;

    private static final String url = "http://minfin.com.ua/data/currency/ib/usd.ib.stock.json";

    public List<CurrencyExchangeRecord> getDataForPeriod(Date startDate, Date endDate) {
        // get data from web site
        ResponseEntity<CurrencyExchangeRecord[]> response = null;
        try {
            response = restTemplate.getForEntity(url, CurrencyExchangeRecord[].class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // filter data with date range
        List<CurrencyExchangeRecord> filteredData = new ArrayList<>();
        if (response != null) {
            List<CurrencyExchangeRecord> responseData = Arrays.asList(response.getBody());
            for (CurrencyExchangeRecord item : responseData) {
                if ((startDate == null || item.getDate().after(startDate))
                        && (endDate == null || item.getDate().before(endDate))) {
                    filteredData.add(item);
                }
            }
        }
        return filteredData;
    }
}
