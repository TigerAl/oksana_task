package service;

import model.CurrencyExchangeRecord;

import java.util.Date;
import java.util.List;

/**
 * Provide API to get a currency exchange history data.
 */
public interface CurrencyExchangeDataProviderService {

    public List<CurrencyExchangeRecord> getDataForPeriod(Date startDate, Date endDate);
}
