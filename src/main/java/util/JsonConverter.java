package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonConverter {

    private Gson gson;

    public JsonConverter(String dateFormatPattern) {
        gson = new GsonBuilder().setDateFormat(dateFormatPattern).create();
    }

    public String toJson(Object object) {
        return gson.toJson(object);
    }

    public <T> T fromJson(String json, Class<T> toClass) {
        return gson.fromJson(json, toClass);
    }
}
