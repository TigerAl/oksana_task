<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
<head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.js"></script>
    <script src="http://code.highcharts.com/stock/highstock.js"></script>
    <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>

    <!-- Chart iniitalization on page load -->
    <script type="text/javascript">

        function onPageLoad() {
            var startDate = ${jsonConverter.toJson(firstAvailableDataDate)};
            var endDate = ${jsonConverter.toJson(lastAvailableDataDate)};
            $.getJSON('data?startDate=' + startDate + '&endDate=' + endDate, function(data) {
                chartData = convertDataToHighchartFormat(data);
                renderChart(chartData);
            });
        }

        /* see http://api.highcharts.com/highstock#series.data for details */
        function convertDataToHighchartFormat(data) {
            var resultData = [];
            for (var index in data) {
                var item = data[index];
                var chartItem = [];
                var date = $.datepicker.parseDate('yy-mm-dd', item.date); // this is a jQuery UI date format which is equal to "yyyy-MM-dd" in Java
                chartItem.push(date.getTime());
                chartItem.push(item.ask);  // open
                chartItem.push(item.ask + Math.random() * 0.1);  // high
                chartItem.push(item.bid - Math.random() * 0.1);  // low
                chartItem.push(item.bid);  // close
                resultData.push(chartItem);
            }
            return resultData;
        }

        function renderChart(data) {
            console.log(JSON.stringify(data));
            $('#chartContainer').highcharts('StockChart', {

                rangeSelector : {
                    inputEnabled: true,
                    selected : 1
                },

                title : {
                    text : 'Our custom CandleStick chart'
                },

                series : [{
                    type : 'candlestick',
                    name : 'AAPL Stock Price',
                    data : data,
                    dataGrouping : {
                        units : [
                            ['week', // unit name
                                [1] // allowed multiples
                            ], [
                                'month',
                                [1, 2, 3, 4, 6]]
                        ]
                    }
                }]
            });
        }
    </script>
</head>
<body onload="onPageLoad()">
    <h1>Chart page</h1>

    <div id="chartContainer" style="height: 400px; min-width: 310px"></div>
</body>
</html>